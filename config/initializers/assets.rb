# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )

Rails.application.config.assets.enabled = true
#Rails.application.config.assets.precompile += ['active_admin.css', 'generic.css', 'ourstyle.css']
Rails.application.config.assets.precompile += [ 'active_admin.css', 'feed_mill.css' ]
#Rails.application.config.autoload_paths += %W(#{Rails.application.config.root}/app/models/ckeditor)
#Rails.application.config.assets.precompile += Ckeditor.assets
#Rails.application.config.assets.precompile += %w(ckeditor/*)

#config.assets.paths << Rails.root.join("app", "assets", "fonts")
Rails.application.config.assets.precompile += %w(.svg .eot .woff .ttf)
Rails.application.config.assets.precompile += %w( ie7.js jquery.js jquery.easing.min.js jquery.validate.min.js polyfiller.js modernizr-custom.js feed_mill.js )

