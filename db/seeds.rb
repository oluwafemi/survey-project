# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

#bin/rake db:seed

MillSkillsRequired.create!(name: 'Good accounting—able to understand and analyze sales figures')
MillSkillsRequired.create!(name: 'Excellent communication and people skills')
MillSkillsRequired.create!(name: 'Able to work under pressure and handle challenging situations')
MillSkillsRequired.create!(name: 'Decision making ability and a sense of responsibility')
MillSkillsRequired.create!(name: 'Negotiate/bargain for better prices')
MillSkillsRequired.create!(name: 'Ability to operate milling machines')
MillSkillsRequired.create!(name: 'Ability to supervise other employees')
MillSkillsRequired.create!(name: 'Others, specify', has_others: true)

MillSkillsLookedFor.create!(name: 'They should be flexible in their working hours')
MillSkillsLookedFor.create!(name: 'Ability to lift heavy materials/objects')
MillSkillsLookedFor.create!(name: 'Ability to work late/early hours')
MillSkillsLookedFor.create!(name: 'Ability to travel distances to procure feed inputs or address other business matters')
MillSkillsLookedFor.create!(name: 'Reliability')
MillSkillsLookedFor.create!(name: 'Gender')
MillSkillsLookedFor.create!(name: 'Marital status')
MillSkillsLookedFor.create!(name: 'Other, specify', has_others: true)

MillAssociation.create!(description: 'Traders association within the market')
MillAssociation.create!(description: 'Traders association outside the market ')
MillAssociation.create!(description: 'Cooperative society outside the market')
MillAssociation.create!(description: 'Cooperative society within the market')
MillAssociation.create!(description: 'Religious association ')
MillAssociation.create!(description: 'Others, specify', has_others: true)

MillPriceInfo.create!(description: 'Fellow feedmillers within the cluster/market')
MillPriceInfo.create!(description: 'Feed millers outside the cluster/market but within the same city')
MillPriceInfo.create!(description: 'Feed millers outside the cluster/market but within the state')
MillPriceInfo.create!(description: 'Feed millers from outside the state')
MillPriceInfo.create!(description: 'Radio')
MillPriceInfo.create!(description: 'Television')
MillPriceInfo.create!(description: 'Millers association in the market/cluster')
MillPriceInfo.create!(description: 'Millers association outside market/cluster')
MillPriceInfo.create!(description: 'Transporters')
MillPriceInfo.create!(description: 'Others, specify', has_others: true)