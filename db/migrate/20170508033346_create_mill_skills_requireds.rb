class CreateMillSkillsRequireds < ActiveRecord::Migration[5.1]
  def change
    create_table :mill_skills_requireds do |t|
      t.string :name
      t.boolean :has_others, default: false
      t.timestamps
    end
  end
end
