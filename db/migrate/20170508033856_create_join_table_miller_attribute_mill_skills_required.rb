class CreateJoinTableMillerAttributeMillSkillsRequired < ActiveRecord::Migration[5.1]
  def change
    create_join_table :miller_attributes, :mill_skills_requireds, table_name: :miller_mill_skills_requireds do |t|
      t.string :others
    end
    add_index :miller_mill_skills_requireds, [:miller_attribute_id, :mill_skills_required_id], unique: true, name: 'miller_requireds_idx'
  end
end
