class CreateMillSkillsLookedFors < ActiveRecord::Migration[5.1]
  def change
    create_table :mill_skills_looked_fors do |t|
      t.string :name
      t.boolean :has_others, default: false
      t.timestamps
    end
  end
end
