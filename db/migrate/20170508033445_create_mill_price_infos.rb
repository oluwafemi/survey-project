class CreateMillPriceInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :mill_price_infos do |t|
      t.string :description
      t.boolean :has_others, default: false
      t.timestamps
    end
  end
end
