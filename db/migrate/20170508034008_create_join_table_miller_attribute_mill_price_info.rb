class CreateJoinTableMillerAttributeMillPriceInfo < ActiveRecord::Migration[5.1]
  def change
    create_join_table :miller_attributes, :mill_price_infos, table_name: :miller_mill_price_infos do |t|
      t.string :others
    end
    add_index :miller_mill_price_infos, [:miller_attribute_id, :mill_price_info_id], unique: true, name: 'miller_price_info_idx'
  end
end
