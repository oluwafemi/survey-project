class CreateJoinTableMillerAttributeMillAssociation < ActiveRecord::Migration[5.1]
  def change
    create_join_table :miller_attributes, :mill_associations, table_name: :miller_mill_associations do |t|
      t.string :others
    end
    add_index :miller_mill_associations, [:miller_attribute_id, :mill_association_id], unique: true, name: 'miller_assoc_idx'
  end
end
