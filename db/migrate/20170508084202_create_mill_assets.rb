class CreateMillAssets < ActiveRecord::Migration[5.1]
  def change
    create_table :mill_assets do |t|
      t.integer :year

      #PUBLIC ASSET ACCESS
      t.integer :distance_to_highway
      t.integer :distance_to_town
      t.string :location_inside_outside
      t.integer :location_outside_distance
      t.boolean :access_to_mill_equipment
      t.boolean :access_for_humidity
      t.boolean :access_to_labs
      t.boolean :access_to_rental_truck
      t.boolean :access_transport_maize
      t.boolean :access_transport_feed

      #11+12=100% Impose Sum restriction
      t.integer :share_water_state
      t.integer :share_water_borehole

      t.integer :electricity_from_grid

      #PRIVATE ASSETS
      t.integer :no_of_trucks_own
      t.string :trucks_own_size
      t.string :trucks_own_size_others
      t.integer :no_of_trucks_rent
      t.string :trucks_rent_size
      t.string :trucks_rent_size_others
      t.integer :motorcycles_own
      t.boolean :motorcycles_pool
      t.integer :maize_dryers_own
      t.integer :capacity_maize_dryers
      t.integer :maize_dryers_rent
      t.integer :maize_vol_dried
      t.integer :sealable_bins_storage
      t.integer :moisture_stick
      t.integer :no_of_cleaners
      t.integer :no_of_hammer_mills
      t.integer :capacity_hammer_mills
      t.integer :no_of_roller_mills
      t.integer :capacity_roller_mills
      t.integer :total_capacity_mill

      #Impose a restriction, value must not be greater than 31
      t.integer :total_utilization_high
      t.integer :total_utilization_low
      t.integer :no_of_extruders
      t.integer :feed_bagging_machine
      t.integer :Packaging_machine

      #Number if 0>>39
      t.integer :no_warehouses_owned_south

      t.integer :capacity_owned_south

      #Number if 0>>41
      t.integer :no_warehouses_rented_south
      t.integer :capacity_rented_south

      #Number if 0>>43
      t.integer :no_warehouses_owned_north

      t.integer :capacity_owned_north

      #Number if 0>>45
      t.integer :no_warehouses_rented_north
      t.integer :capacity_rented_north

      t.integer :conveyor_belts_bags
      t.integer :feed_selling_shops_south
      t.integer :feed_selling_shops_north
      t.integer :no_of_cell_phones
      t.integer :no_of_computers
      t.integer :no_of_fax_machines
      t.integer :no_of_gen_working
      t.integer :capacity_of_gen_working
      t.integer :no_of_solar_working
      t.integer :no_of_batteries_working
      t.integer :capacity_of_solar_batt_working

      t.timestamps
    end
    add_index :mill_assets, :year, unique: true
  end
end