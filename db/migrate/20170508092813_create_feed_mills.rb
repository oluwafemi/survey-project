class CreateFeedMills < ActiveRecord::Migration[5.1]
  def change
    create_table :feed_mills do |t|
      t.date :interview_date
      t.string :state
      t.string :lga
      t.string :ward
      t.string :community
      t.string :name_of_miller, null: false
      t.references :admin_user, null: false, index: true, foreign_key: true
      t.references :miller_attribute, null: false, index: true, foreign_key: true
      t.references :mill_activity, null: false, index: true, foreign_key: true
      t.references :mill_asset, null: false, index: true, foreign_key: true
      t.timestamps
    end
  end
end
