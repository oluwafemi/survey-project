class CreateMillerAttributes < ActiveRecord::Migration[5.1]
  def change
    create_table :miller_attributes do |t|

      #PERSONAL ATTRIBUTES
      t.integer :age
      t.string :gender
      t.string :marital
      t.string :education
      t.string :religion
      t.string :origin_state
      t.string :job_in_nigeria_not_miller_sixteen
      t.string :job_in_nigeria_not_miller_eleven
      t.string :job_in_nigeria_before_milling
      t.boolean :ever_migrated_outside_nigeria
      t.integer :year_migrated
      t.integer :year_returned
      t.string :where_migrated_to
      t.boolean :have_hired_mill_manager
      t.integer :managers_in_past_three_years
      t.integer :how_many_females
      #MillSkillsRequired has_and_belongs_to_many
      #MillSkillsLookedFor has_and_belongs_to_many

      #MANAGER ATTRIBUTES
      t.integer :manager_age
      t.string :manager_gender
      t.string :manager_education

      #MILL CHARACTERISTICS
      #MillManager has_manys with just gender attr
      t.integer :times_owner_visits
      t.integer :how_long_owner_stays
      t.string :owvership_status

      #FEEDMILLING EXPERIENCE
      t.integer :year_started_milling
      t.integer :year_got_first_mill
      t.boolean :training_in_mill_storage
      t.string :training_from_whom      
      #MillAssociation has_and_belongs_to_many
      t.string :role_in_association
      t.boolean :earnings_for_startup
      t.boolean :earnings_for_opex
      t.string :startup_funds_source
      t.boolean :feed_connected_to_phcn
      t.boolean :informal_gift_requested
      t.integer :how_long_for_water
      #MillPriceInfo has_and_belongs_to_many

      #OTHER MAIZE AND FEED RELATED ACTIVITY
      t.boolean :owner_grow_maize
      t.integer :years_grown_maize
      t.boolean :owner_grow_soy_beans
      t.integer :years_grown_soy_beans
      t.boolean :owner_engage_maize_wholesale
      t.integer :years_engage_maize_wholesale
      t.boolean :owner_feed_wholesaler
      t.integer :years_feed_wholesaler
      t.boolean :owner_feed_retailer
      t.integer :years_feed_retailer

      t.timestamps
    end
  end
end
