class CreateMillActivities < ActiveRecord::Migration[5.1]
  def change
    create_table :mill_activities do |t|

      t.integer :year

      #EXTENT OF OTHER MAIZE & FEED RELATED ACTIVITY OVER TIME
      t.integer :land_to_maize
      t.integer :land_to_soybean
      t.integer :vol_maize_traded_wholesale
      t.integer :vol_feed_traded_wholesale
      t.integer :vol_feed_traded_retailer

      #MILL PRODUCTS 
      #6-12 should sum to 100% Impose sum restriction
      t.integer :share_total_chicken_feed
      t.integer :share_total_fish_feed
      t.integer :share_total_pig_feed
      t.integer :share_total_cattle_feed
      t.integer :share_total_maize_human
      t.integer :share_total_wheat_human

      #MILL INGREDIENTS FOR CHICKEN FEED
      #3-21 should sum to approx. 1000kg
      t.integer :maize
      t.integer :maize_bran
      t.integer :soybean_meal
      t.integer :sorghum
      t.integer :fish_meal
      t.integer :peanut_cake
      t.integer :sunflower_cake
      t.integer :cotton_seed_cake
      t.string :other_main_ingredient
      t.integer :other_main_ingredient_kg

      #ACTIVITIES of MILL
      #22-24 should sum to 100% Impose Sum restriction
      t.integer :share_milling_done_feed
      t.integer :share_milling_done_flour
      t.integer :share_milling_not_done

      t.integer :maize_in_total_volume
      t.boolean :collect_maize_from_farmers
      t.boolean :collect_soy_from_farmers
      t.boolean :collect_sorghum_from_farmers
      t.boolean :secure_soybeans_on_contract
      t.string :first_contract_type
      t.string :first_contract_type_others

      t.boolean :get_maize_from_wholesale
      t.boolean :transporter_gets_maize_for_you
      t.boolean :secure_maize_on_contract
      t.string :second_contract_type
      t.string :second_contract_type_others

      t.boolean :buy_maize_for_humidity
      t.boolean :test_maize_for_fungus
      t.boolean :dry_maize_procure
      t.boolean :store_maize_warehouse
      t.boolean :treat_maize_chemical
      t.boolean :add_pepper_to_maize
      t.boolean :get_rid_of_dirt_in_maize
      t.boolean :mixing_ingredients
      t.boolean :extrusion_of_feed
      t.boolean :bag_feed
      t.boolean :label_feed_sack
      t.boolean :brand_feed_sack
      t.boolean :store_feed_warehouse
      t.boolean :retail_feed
      t.boolean :deliver_feed_to_buyer
      t.boolean :sell_feed_contractual
      t.string :third_contract_type
      t.string :third_contract_type_others
      t.boolean :sell_feed_outside_nigeria
      t.boolean :sell_feed_outside_africa

      t.timestamps
    end
    add_index :mill_activities, :year, unique: true
  end
end