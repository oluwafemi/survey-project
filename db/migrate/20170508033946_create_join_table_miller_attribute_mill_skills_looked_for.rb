class CreateJoinTableMillerAttributeMillSkillsLookedFor < ActiveRecord::Migration[5.1]
  def change
    create_join_table :miller_attributes, :mill_skills_looked_fors, table_name: :miller_mill_skills_looked_fors do |t|
      t.string :others
    end
    add_index :miller_mill_skills_looked_fors, [:miller_attribute_id, :mill_skills_looked_for_id], unique: true, name: 'miller_looked_for_idx'
  end
end
