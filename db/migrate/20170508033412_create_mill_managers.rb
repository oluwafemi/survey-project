class CreateMillManagers < ActiveRecord::Migration[5.1]
  def change
    create_table :mill_managers do |t|
      t.string :gender
      t.timestamps
    end
  end
end
