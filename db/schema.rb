# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170508092813) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "user_type", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["first_name", "last_name"], name: "admin_user_first_last_name_idx", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "feed_mills", force: :cascade do |t|
    t.date "interview_date"
    t.string "state"
    t.string "lga"
    t.string "ward"
    t.string "community"
    t.string "name_of_miller", null: false
    t.bigint "admin_user_id", null: false
    t.bigint "miller_attribute_id", null: false
    t.bigint "mill_activity_id", null: false
    t.bigint "mill_asset_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["admin_user_id"], name: "index_feed_mills_on_admin_user_id"
    t.index ["mill_activity_id"], name: "index_feed_mills_on_mill_activity_id"
    t.index ["mill_asset_id"], name: "index_feed_mills_on_mill_asset_id"
    t.index ["miller_attribute_id"], name: "index_feed_mills_on_miller_attribute_id"
  end

  create_table "mill_activities", force: :cascade do |t|
    t.integer "year"
    t.integer "land_to_maize"
    t.integer "land_to_soybean"
    t.integer "vol_maize_traded_wholesale"
    t.integer "vol_feed_traded_wholesale"
    t.integer "vol_feed_traded_retailer"
    t.integer "share_total_chicken_feed"
    t.integer "share_total_fish_feed"
    t.integer "share_total_pig_feed"
    t.integer "share_total_cattle_feed"
    t.integer "share_total_maize_human"
    t.integer "share_total_wheat_human"
    t.integer "maize"
    t.integer "maize_bran"
    t.integer "soybean_meal"
    t.integer "sorghum"
    t.integer "fish_meal"
    t.integer "peanut_cake"
    t.integer "sunflower_cake"
    t.integer "cotton_seed_cake"
    t.string "other_main_ingredient"
    t.integer "other_main_ingredient_kg"
    t.integer "share_milling_done_feed"
    t.integer "share_milling_done_flour"
    t.integer "share_milling_not_done"
    t.integer "maize_in_total_volume"
    t.boolean "collect_maize_from_farmers"
    t.boolean "collect_soy_from_farmers"
    t.boolean "collect_sorghum_from_farmers"
    t.boolean "secure_soybeans_on_contract"
    t.string "first_contract_type"
    t.string "first_contract_type_others"
    t.boolean "get_maize_from_wholesale"
    t.boolean "transporter_gets_maize_for_you"
    t.boolean "secure_maize_on_contract"
    t.string "second_contract_type"
    t.string "second_contract_type_others"
    t.boolean "buy_maize_for_humidity"
    t.boolean "test_maize_for_fungus"
    t.boolean "dry_maize_procure"
    t.boolean "store_maize_warehouse"
    t.boolean "treat_maize_chemical"
    t.boolean "add_pepper_to_maize"
    t.boolean "get_rid_of_dirt_in_maize"
    t.boolean "mixing_ingredients"
    t.boolean "extrusion_of_feed"
    t.boolean "bag_feed"
    t.boolean "label_feed_sack"
    t.boolean "brand_feed_sack"
    t.boolean "store_feed_warehouse"
    t.boolean "retail_feed"
    t.boolean "deliver_feed_to_buyer"
    t.boolean "sell_feed_contractual"
    t.string "third_contract_type"
    t.string "third_contract_type_others"
    t.boolean "sell_feed_outside_nigeria"
    t.boolean "sell_feed_outside_africa"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["year"], name: "index_mill_activities_on_year", unique: true
  end

  create_table "mill_assets", force: :cascade do |t|
    t.integer "year"
    t.integer "distance_to_highway"
    t.integer "distance_to_town"
    t.string "location_inside_outside"
    t.integer "location_outside_distance"
    t.boolean "access_to_mill_equipment"
    t.boolean "access_for_humidity"
    t.boolean "access_to_labs"
    t.boolean "access_to_rental_truck"
    t.boolean "access_transport_maize"
    t.boolean "access_transport_feed"
    t.integer "share_water_state"
    t.integer "share_water_borehole"
    t.integer "electricity_from_grid"
    t.integer "no_of_trucks_own"
    t.string "trucks_own_size"
    t.string "trucks_own_size_others"
    t.integer "no_of_trucks_rent"
    t.string "trucks_rent_size"
    t.string "trucks_rent_size_others"
    t.integer "motorcycles_own"
    t.boolean "motorcycles_pool"
    t.integer "maize_dryers_own"
    t.integer "capacity_maize_dryers"
    t.integer "maize_dryers_rent"
    t.integer "maize_vol_dried"
    t.integer "sealable_bins_storage"
    t.integer "moisture_stick"
    t.integer "no_of_cleaners"
    t.integer "no_of_hammer_mills"
    t.integer "capacity_hammer_mills"
    t.integer "no_of_roller_mills"
    t.integer "capacity_roller_mills"
    t.integer "total_capacity_mill"
    t.integer "total_utilization_high"
    t.integer "total_utilization_low"
    t.integer "no_of_extruders"
    t.integer "feed_bagging_machine"
    t.integer "Packaging_machine"
    t.integer "no_warehouses_owned_south"
    t.integer "capacity_owned_south"
    t.integer "no_warehouses_rented_south"
    t.integer "capacity_rented_south"
    t.integer "no_warehouses_owned_north"
    t.integer "capacity_owned_north"
    t.integer "no_warehouses_rented_north"
    t.integer "capacity_rented_north"
    t.integer "conveyor_belts_bags"
    t.integer "feed_selling_shops_south"
    t.integer "feed_selling_shops_north"
    t.integer "no_of_cell_phones"
    t.integer "no_of_computers"
    t.integer "no_of_fax_machines"
    t.integer "no_of_gen_working"
    t.integer "capacity_of_gen_working"
    t.integer "no_of_solar_working"
    t.integer "no_of_batteries_working"
    t.integer "capacity_of_solar_batt_working"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["year"], name: "index_mill_assets_on_year", unique: true
  end

  create_table "mill_associations", force: :cascade do |t|
    t.string "description"
    t.boolean "has_others", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mill_managers", force: :cascade do |t|
    t.string "gender"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mill_price_infos", force: :cascade do |t|
    t.string "description"
    t.boolean "has_others", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mill_skills_looked_fors", force: :cascade do |t|
    t.string "name"
    t.boolean "has_others", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mill_skills_requireds", force: :cascade do |t|
    t.string "name"
    t.boolean "has_others", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "miller_attributes", force: :cascade do |t|
    t.integer "age"
    t.string "gender"
    t.string "marital"
    t.string "education"
    t.string "religion"
    t.string "origin_state"
    t.string "job_in_nigeria_not_miller_sixteen"
    t.string "job_in_nigeria_not_miller_eleven"
    t.string "job_in_nigeria_before_milling"
    t.boolean "ever_migrated_outside_nigeria"
    t.integer "year_migrated"
    t.integer "year_returned"
    t.string "where_migrated_to"
    t.boolean "have_hired_mill_manager"
    t.integer "managers_in_past_three_years"
    t.integer "how_many_females"
    t.integer "manager_age"
    t.string "manager_gender"
    t.string "manager_education"
    t.integer "times_owner_visits"
    t.integer "how_long_owner_stays"
    t.string "owvership_status"
    t.integer "year_started_milling"
    t.integer "year_got_first_mill"
    t.boolean "training_in_mill_storage"
    t.string "training_from_whom"
    t.string "role_in_association"
    t.boolean "earnings_for_startup"
    t.boolean "earnings_for_opex"
    t.string "startup_funds_source"
    t.boolean "feed_connected_to_phcn"
    t.boolean "informal_gift_requested"
    t.integer "how_long_for_water"
    t.boolean "owner_grow_maize"
    t.integer "years_grown_maize"
    t.boolean "owner_grow_soy_beans"
    t.integer "years_grown_soy_beans"
    t.boolean "owner_engage_maize_wholesale"
    t.integer "years_engage_maize_wholesale"
    t.boolean "owner_feed_wholesaler"
    t.integer "years_feed_wholesaler"
    t.boolean "owner_feed_retailer"
    t.integer "years_feed_retailer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "miller_mill_associations", id: false, force: :cascade do |t|
    t.bigint "miller_attribute_id", null: false
    t.bigint "mill_association_id", null: false
    t.string "others"
    t.index ["miller_attribute_id", "mill_association_id"], name: "miller_assoc_idx", unique: true
  end

  create_table "miller_mill_price_infos", id: false, force: :cascade do |t|
    t.bigint "miller_attribute_id", null: false
    t.bigint "mill_price_info_id", null: false
    t.string "others"
    t.index ["miller_attribute_id", "mill_price_info_id"], name: "miller_price_info_idx", unique: true
  end

  create_table "miller_mill_skills_looked_fors", id: false, force: :cascade do |t|
    t.bigint "miller_attribute_id", null: false
    t.bigint "mill_skills_looked_for_id", null: false
    t.string "others"
    t.index ["miller_attribute_id", "mill_skills_looked_for_id"], name: "miller_looked_for_idx", unique: true
  end

  create_table "miller_mill_skills_requireds", id: false, force: :cascade do |t|
    t.bigint "miller_attribute_id", null: false
    t.bigint "mill_skills_required_id", null: false
    t.string "others"
    t.index ["miller_attribute_id", "mill_skills_required_id"], name: "miller_requireds_idx", unique: true
  end

  add_foreign_key "feed_mills", "admin_users"
  add_foreign_key "feed_mills", "mill_activities"
  add_foreign_key "feed_mills", "mill_assets"
  add_foreign_key "feed_mills", "miller_attributes"
end
