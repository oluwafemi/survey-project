module ApplicationHelper
  
  APP_ADMINISTRATOR = "App Administrator"

  HOST_ADMIN_USERNAME = 'HOST ADMIN'

  SUPERUSER_TYPE = "SUPERUSER"

  ADMINISTRATOR_TYPE = "ADMINISTRATOR"

  REGULAR_USER_TYPE = "REGULAR USER"

  ALL_USER_TYPES = [SUPERUSER_TYPE, ADMINISTRATOR_TYPE, REGULAR_USER_TYPE]

  def hidden_div_if(condition, attributes = {}, &block)
   	if condition
   		attributes["style"] = "display: none"
   	end
    	content_tag("div", attributes, &block)
  end

end