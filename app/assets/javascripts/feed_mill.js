//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

var current_state_index = 0;
var active_ward_index = 0;
var active_comm_index = 0;

var state_indexes_to_lga_div_ids = ["#four_one", "#four_two", "#four_three", "#four_four"];

var lga_indexes_to_wards_div_ids = [];
lga_indexes_to_wards_div_ids.push(["#five_one_one", "#five_one_two", "#five_one_three", "#five_one_four"]);
lga_indexes_to_wards_div_ids.push(["#five_two_one", "#five_two_two", "#five_two_three", "#five_two_four"]);
lga_indexes_to_wards_div_ids.push(["#five_three_one", "#five_three_two", "#five_three_three", "#five_three_four"]);
lga_indexes_to_wards_div_ids.push(["#five_four_one", "#five_four_two", "#five_four_three", "#five_four_four"]);

var first_ward_of_wards = [];
first_ward_of_wards.push(["#six_one_one_one", "#six_one_one_two", "#six_one_one_three"]);
first_ward_of_wards.push(["#six_one_two_one", "#six_one_two_two", "#six_one_two_three"]);
first_ward_of_wards.push(["#six_one_three_one", "#six_one_three_two", "#six_one_three_three"]);
first_ward_of_wards.push(["#six_one_four_one", "#six_one_four_two", "#six_one_four_three"]);

var second_ward_of_wards = [];
second_ward_of_wards.push(["#six_two_one_one", "#six_two_one_two", "#six_two_one_three"]);
second_ward_of_wards.push(["#six_two_two_one", "#six_two_two_two", "#six_two_two_three"]);
second_ward_of_wards.push(["#six_two_three_one", "#six_two_three_two", "#six_two_three_three"]);
second_ward_of_wards.push(["#six_two_four_one", "#six_two_four_two", "#six_two_four_three"]);

var third_ward_of_wards = [];
third_ward_of_wards.push(["#six_three_one_one", "#six_three_one_two", "#six_three_one_three"]);
third_ward_of_wards.push(["#six_three_two_one", "#six_three_two_two", "#six_three_two_three"]);
third_ward_of_wards.push(["#six_three_three_one", "#six_three_three_two", "#six_three_three_three"]);
third_ward_of_wards.push(["#six_three_four_one", "#six_three_four_two", "#six_three_four_three"]);

var fourth_ward_of_wards = [];
fourth_ward_of_wards.push(["#six_four_one_one", "#six_four_one_two", "#six_four_one_three"]);
fourth_ward_of_wards.push(["#six_four_two_one", "#six_four_two_two", "#six_four_two_three"]);
fourth_ward_of_wards.push(["#six_four_three_one", "#six_four_three_two", "#six_four_three_three"]);
fourth_ward_of_wards.push(["#six_four_four_one", "#six_four_four_two", "#six_four_four_three"]);

var ward_indexes_to_comms_div_ids = [];
ward_indexes_to_comms_div_ids.push(first_ward_of_wards);
ward_indexes_to_comms_div_ids.push(second_ward_of_wards);
ward_indexes_to_comms_div_ids.push(third_ward_of_wards);
ward_indexes_to_comms_div_ids.push(fourth_ward_of_wards);

function display_active_lgas(state_natural_idx) {
    current_state_index = (state_natural_idx - 1);
    for (i = 0; i < state_indexes_to_lga_div_ids.length; i++) {
        var element = $(state_indexes_to_lga_div_ids[i]);
        if (i === current_state_index) {
            element.show();
        } else {
            element.hide();
        }
    }
}

function display_active_wards() {
    for (i = 0; i < lga_indexes_to_wards_div_ids.length; i++) {
        for (j = 0; j < lga_indexes_to_wards_div_ids[i].length; j++) {
            var element = $(lga_indexes_to_wards_div_ids[i][j]);
            if ((i === current_state_index) && (j === active_ward_index)) {
                element.show();
            } else {
                element.hide();
            }
        }
    }
}

function display_active_comms() {
    for (i = 0; i < ward_indexes_to_comms_div_ids.length; i++) {
        for (j = 0; j < ward_indexes_to_comms_div_ids[i].length; j++) {
            for (k = 0; k < ward_indexes_to_comms_div_ids[i][j].length; k++) {
                var element = $(ward_indexes_to_comms_div_ids[i][j][k]);
                if ((i === current_state_index) && (j === active_ward_index) &&
                    (k === active_comm_index)) {
                    element.show();
                } else {
                    element.hide();
                }
            }
        }
    }
}

$(".next").click(function() {

    if (animating) return false;
    var validator = $("#msform").validate();
    var elem_id = $(this).attr('data-element-id')

    var active_element = $(elem_id);

    if (active_element.hasClass('test-for-blank')) {
        var trimmedValue = jQuery.trim(active_element.val());
        if (trimmedValue.length === 0) {
            return false;
        }
    }

    if (($(this).attr('data-next-in-seq') === "#five") ||
        ($(this).attr('data-next-in-seq') === "#six")||
        ($(this).attr('data-next-in-seq') === "#seven") ||
        (active_element.hasClass("div-for-generic-radios"))) {
        //
    } else {        
        validator.element(active_element);    
        if (!validator.valid()) {
            return false;
        }
    }
    
    animating = true;

    var next_fs_id = $(this).attr('data-next-in-seq');

    if (active_element.attr("id") == "wish_to_continue_No") {
        if(active_element.is(':checked')) { 
            //window.location.href = window.location.origin;
            window.location = '/';
        }
    }
        
    current_fs = $(this).parent();

    //next_fs = $(this).parent().next();
    next_fs = $(next_fs_id);

    if (next_fs.attr("id") === "four") {
        display_active_lgas(parseInt(active_element.val()));
    }
    else if (next_fs.attr("id") === "five") {
        var lga_element = $(state_indexes_to_lga_div_ids[current_state_index]);
        var children_length = lga_element.children(":radio").length;
        active_ward_index = -1;
        lga_element.children(":radio").each(function(index, value) {
            active_ward_index += 1;
            if ($(this).is(':checked')) {
                display_active_wards();
                return false;
            }
        });
    }
    else if (next_fs.attr("id") === "six") {
        var ward_element = $(lga_indexes_to_wards_div_ids[current_state_index][active_ward_index]);
        var children_length = ward_element.children(":radio").length;
        active_comm_index = -1;
        ward_element.children(":radio").each(function(index, value) {
            active_comm_index += 1;
            if ($(this).is(':checked')) {
                display_active_comms();
                return false;
            }
        });
    }
    
    //activate next step on progressbar using the index of next_fs
    //$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
    
    //show the next fieldset
    next_fs.show(); 
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50)+"%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'transform': 'scale('+scale+')'});
            next_fs.css({'left': left, 'opacity': opacity});
        }, 
        duration: 800, 
        complete: function(){
            current_fs.hide();
            animating = false;
        }, 
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".previous").click(function(){
    if(animating) return false;
    animating = true;

    var prev_fs_id = $(this).attr('data-next-in-seq');
    
    current_fs = $(this).parent();
    //previous_fs = $(this).parent().prev();
    previous_fs = $(prev_fs_id);

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
    
    //show the previous fieldset
    previous_fs.show(); 
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1-now) * 50)+"%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
        }, 
        duration: 800, 
        complete: function(){
            current_fs.hide();
            animating = false;
        }, 
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".submit").click(function(){
    return false;
})

  //.closest( "ul" )
/*$(this).siblings(".div-for-radios").each(function() {
            index += 1;
            if ($(this).css('display') !== 'none') {
                get_index_of_selected_lga(index);
                break;
            }
            index += $(this).children(".label-for-radios").length;
        }*/