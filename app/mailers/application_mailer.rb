class ApplicationMailer < ActionMailer::Base
  default from: 'oluwafemiadenubi@outlook.com'
  layout 'mailer'
end
