class MillerAttribute < ApplicationRecord
  has_and_belongs_to_many :mill_skills_requireds
  has_and_belongs_to_many :mill_skills_looked_fors
  has_many :mill_managers
  has_and_belongs_to_many :mill_associations
  has_and_belongs_to_many :mill_price_infos
  #we need to whitelist mill_skills_requireds_ids and vice versa in the controller
  #params.require(:miller_attribute).permit(:own_attributes, mill_skills_requireds_ids:[])
end
