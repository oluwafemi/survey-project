class MillAssociation < ApplicationRecord
  has_and_belongs_to_many :miller_attributes
  validates :description, presence: true
  def mill_association_params
    params[:mill_association].permit(:description, :has_others)
  end
end
