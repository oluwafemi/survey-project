class FeedMill < ApplicationRecord
  belongs_to :admin_user
  has_one :miller_attribute
  has_many :mill_activities
  has_many :mill_assets

  attr_accessor :wish_to_continue

  default_scope { order("id") }

  validates :interview_date, :state, :lga, :ward, :community, :admin_user_id, :miller_attribute_id, :mill_activity_id, :mill_asset_id, presence: true
  validates :name_of_miller, presence: true, uniqueness: { message: "Miller name already exist" }

  def feed_mill_params 
    params.require(:feed_mill).permit(:interview_date, :state, :lga, :city, :name_of_miller, :admin_user_id, :miller_attribute_id, :mill_activity_id, :mill_asset_id)
  end

end