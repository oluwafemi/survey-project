class MillPriceInfo < ApplicationRecord
  has_and_belongs_to_many :miller_attributes
  validates :description, presence: true
  def mill_price_info_params
    params[:mill_price_info].permit(:description, :has_others)
  end
end
