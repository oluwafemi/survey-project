class MillManager < ApplicationRecord
  belongs_to :miller_attributes
  validates :gender, presence: true
  def mill_manager_params
    params[:mill_manager].permit(:gender)
  end
end
