class MillSkillsLookedFor < ApplicationRecord
  has_and_belongs_to_many :miller_attributes
  validates :name, presence: true
  def mill_skills_looked_for_params
    params[:mill_skills_looked_for].permit(:name, :has_others)
  end
end
