include ActiveAdminHelpers

class AdminUser < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable, :confirmable,
  default_scope { order("last_name, first_name") }

  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable

  after_create { |admin| admin.send_reset_password_instructions }

  before_validation :normalize_full_names, on: [:create, :update]

  after_destroy :ensure_an_admin_remains

  #before_destroy :ensure_cant_delete_self

  #before_save :validate_user_type

  validates :last_name, :email, :user_type, presence: true

  validates :first_name, presence: true, uniqueness: { scope: :last_name, message: "Field Officer user name already exist" }

  def admin_user_params 
    params.require(:admin_user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :remember_me, :user_type)
  end
  
  def password_required?
    new_record? ? false : super
  end

  def current_title
    "#{self.full_name} | Field Officer"
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def host_admin_user?
    self.full_name == HOST_ADMIN_USERNAME
  end

  def superuser?
    self.user_type == SUPERUSER_TYPE
  end

  def administrator?
    self.user_type == ADMINISTRATOR_TYPE
  end

  def regular_user?
    self.user_type == REGULAR_USER_TYPE
  end

  def administrative_user?
    self.superuser? or self.administrator?
  end

  def same_user?(admin_user)
    self.id == admin_user.id
  end

  def host_company_user?
    self.company_id == HOST_COMPANY_ID
  end

  def has_batch_priveleges?
    self.superuser? or self.administrator?
  end

  protected
    def normalize_full_names
      self.first_name = self.first_name.upcase #self.first_name.downcase.titleize
      self.last_name = self.last_name.upcase
      #self.password = self.email
      #self.password_confirmation = self.email
    end

  private
    def ensure_an_admin_remains
      if AdminUser.count.zero?
        raise "Can't delete last Field Officer"
      end
    end

    def ensure_cant_delete_self
      #if self.id = 
      #  raise "Can't delete yourself"
      #end
    end
end

