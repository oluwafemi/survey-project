class MillSkillsRequired < ApplicationRecord
  has_and_belongs_to_many :miller_attributes
  validates :name, presence: true
  def mill_skills_required_params
    params[:mill_skills_required].permit(:name, :has_others)
  end
end
