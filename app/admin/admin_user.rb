include ActiveAdminHelpers

ActiveAdmin.register AdminUser do
  permit_params :first_name, :last_name, :email, :user_type, :password, :password_confirmation
  actions :all
  config.batch_actions = false

  #batch_action :destroy, :if => proc { current_admin_user.has_batch_priveleges? } do |selection|
    #redirect_to collection_path, :alert => "Didn't really delete these!"
  #end

  config.clear_action_items!
  config.filters = true
  config.clear_sidebar_sections!
  scope_to :current_admin_user, unless: proc{ current_admin_user.administrative_user? }
 
  menu :if => proc{ current_admin_user.administrative_user? }

  action_item only: [:show, :index] do
    links = link_to "New Field Officer", new_admin_admin_user_path
  end

  show :title => :current_title do |admin_user|
    attributes_table do
      if admin_user.encrypted_password == ''
        row :first_name
        row :last_name
        row :email
      else
        if admin_user.host_admin_user?
          row :first_name
        else
          row 'First Name' do 
            text = "<a href='#{edit_admin_admin_user_path(admin_user.id)}' style='text-decoration: none'>#{admin_user.first_name}</a>"
            text.html_safe
          end
        end

        if admin_user.host_admin_user?
          row :last_name
        else
          row 'Last Name' do 
            text = "<a href='#{edit_admin_admin_user_path(admin_user.id)}' style='text-decoration: none'>#{admin_user.last_name}</a>"
            text.html_safe
          end
        end

        row 'Email Address' do 
          text = "<a href='#{edit_admin_admin_user_path(admin_user.id)}' style='text-decoration: none'>#{admin_user.email}</a>"
          text.html_safe
        end

      end
      row :user_type
      row :sign_in_count
      row :current_sign_in_at
      row :last_sign_in_at
      if current_admin_user.superuser?
        row :current_sign_in_ip
        row :last_sign_in_ip
        row :created_at
        row :updated_at
      end
    end
    #if current_admin_user.superuser? active_admin_comments end
  end

  controller do

    def action_methods
      if current_admin_user.administrative_user?
        super
      end
    end

    def edit
      @page_title = "Edit Officer Details"
    end

    def scoped_collection
      # some stuffs
      super.order(:first_name, :last_name)
    end

  end

  menu if: proc{ current_admin_user.administrative_user? }

  index :title => 'Field Officers' do
    #selectable_column
    column "Officer User Name" do  |admin_user|
      link_to admin_user.full_name, admin_admin_user_path(admin_user.id), style: 'text-decoration: none'
    end
    column "Email Address" do  |admin_user|
      link_to admin_user.email, admin_admin_user_path(admin_user.id), style: 'text-decoration: none'
    end
    column :last_sign_in_at
    column :sign_in_count
    column :user_type
    column do |admin_user|
      if admin_user.host_admin_user?
        link_to("DETAILS", admin_admin_user_path(admin_user), style: 'text-decoration: none')
      elsif admin_user.encrypted_password == ''
        delete = current_admin_user.same_user?(admin_user) ? "" :
          " | " + link_to("DELETE", admin_admin_user_path(admin_user), style: 'text-decoration: none', :method => :delete, :confirm => "Are you sure?") 
      
        link_to("DETAILS", admin_admin_user_path(admin_user), style: 'text-decoration: none') + delete.try(:html_safe)
      else
        delete = (current_admin_user.administrative_user? and not current_admin_user.same_user?(admin_user)) ? (" | " + \
          link_to("DELETE", admin_admin_user_path(admin_user), style: 'text-decoration: none', :method => :delete, :confirm => "Are you sure?")) : ""
      
        link_to("DETAILS", admin_admin_user_path(admin_user), style: 'text-decoration: none') + " | " + \
        link_to("EDIT", edit_admin_admin_user_path(admin_user), style: 'text-decoration: none') + delete.try(:html_safe)
      end
    end
  end

  filter :email
  filter :user_type
  filter :current_sign_in_at
  filter :last_sign_in_at
  filter :sign_in_count

  form do |f|
    f.inputs "Field Officer Details" do
      f.input :first_name, :input_html => { :size => 70 }
      f.input :last_name, :input_html => { :size => 70 }
      f.input :email, :as => :email, :input_html => { :size => 70 }
      if current_admin_user.superuser?
        f.input :user_type, :as => :select, :include_blank => (not f.object.persisted?), :collection => ALL_USER_TYPES
      else
        f.input :user_type, :as => :select, :include_blank => false, :collection => (f.object.persisted? ? [f.object.user_type] : [REGULAR_USER_TYPE])
      end
    end
    f.actions
  end
end
