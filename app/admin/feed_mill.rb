ActiveAdmin.register FeedMill do
  permit_params :interview_date, :state, :lga, :city, :name_of_miller, :admin_user_id, :miller_attribute_id, :mill_activity_id, :mill_asset_id
  config.batch_actions = false

  config.clear_action_items!
  config.sort_order = "id_asc"
  config.filters = true
  config.clear_sidebar_sections!

  #show do 
   #within @head do
    #  script :src => javascript_path('somefile.js'), :type => "text/javascript"
   #end
  #end

  form do |f|  	
  	render partial: 'feed_mill'
  end
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

end
